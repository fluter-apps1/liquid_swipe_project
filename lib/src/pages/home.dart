import 'package:flutter/material.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
import 'package:liquid_swipe_project/src/widgets/liquid_pages.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: LiquidSwipe(
      pages: pages,
      fullTransitionValue: 300,
      enableLoop: true,
      enableSlideIcon: true,
      positionSlideIcon: 0.8,
      waveType: WaveType.circularReveal,
      onPageChangeCallback: (page) => pageChangeCallback(page),
      currentUpdateTypeCallback: (updateType) => updateTypeCallback(updateType),
    ));
  }

  pageChangeCallback(int page) {
    print(page);
  }

  updateTypeCallback(UpdateType updateType) {
    
  }
}
