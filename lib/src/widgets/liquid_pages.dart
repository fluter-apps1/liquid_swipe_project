import 'package:flutter/material.dart';

final pages = [
  Container(
    color: Colors.blueAccent,
    child: Center(child: Text('Slide 1')),
  ),
  Container(
    color: Colors.black,
    child: Center(child: Text('Slide 2')),
  ),
  Container(
    color: Colors.amberAccent,
    child: Center(child: Text('Slide 3')),
  ),
];
